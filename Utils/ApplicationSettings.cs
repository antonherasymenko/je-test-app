﻿using System;
using System.Web.Configuration;

namespace JustEat.Utils
{
    public static class ApplicationSettings
    {
        private static string _justItApiUrl;
        public static string JustItApiUrl
        {
            get { return _justItApiUrl ?? (_justItApiUrl = WebConfigurationManager.AppSettings["apiUrl"]); }
        }

        public static string RestaurantApiUrl
        {
            get { return String.Format("{0}/restaurants", JustItApiUrl); }
        }

        public static string MenuApiUrl(string restaurantID)
        {
            return String.Format("{0}/{1}/menus", RestaurantApiUrl, restaurantID);
        }

        public static string ProductListApiUrl(string menuID, string categoryID)
        {
            return String.Format("{0}menus/{1}/productcategories/{2}/products", JustItApiUrl, menuID, categoryID);
        }

        public static string CategoryListApiUrl(string menuID)
        {
            return String.Format("{0}menus/{1}/productcategories", JustItApiUrl, menuID);
        }
    }
}