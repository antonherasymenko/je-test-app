﻿using System.Collections.Generic;

namespace JustEat.Domain.Entities
{
    public class RestaurantInfo
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string AvarageRating { get; set; }
        public string Logo { get; set; }
        public string WebsiteUrl { get; set; }
        public IEnumerable<string> Products { get; set; }
    }
}