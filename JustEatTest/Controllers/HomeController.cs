﻿using System.Web.Mvc;

namespace JustEatTest.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
