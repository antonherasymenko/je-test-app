﻿using System.Web.Mvc;
using JustEat.Services.ProductServices;
using Newtonsoft.Json;

namespace JustEatTest.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductService _productService = new ProductService();

        public ActionResult List(string restaurantID)
        {
            var list = _productService.LoadProductNames(restaurantID);
            return Content(JsonConvert.SerializeObject(list));
        }
    }
}
