﻿using System.Web.Mvc;
using JustEat.Services.RestaurantServices;
using Newtonsoft.Json;

namespace JustEatTest.Controllers
{
    public class RestaurantController : Controller
    {
        readonly IRestaurantService _restaurantService = new RestaurantService();

        public ActionResult List(string postCode)
        {
            var list = _restaurantService.RestaurantList(postCode);
            return Content(JsonConvert.SerializeObject(list));
        }
    }
}
