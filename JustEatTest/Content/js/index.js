﻿var block = function () {
    $.blockUI({
        message: '<img src="/Content/images/ajax_loader_gray_32.gif" />',
        css: { width: "34px;" },
    });
};
var unBlock = function () {
    $.unblockUI();
};

$(document).ajaxStart(block).ajaxStop(unBlock);

var showRestaurants = function () {
    $.ajax({
        type: 'get',
        url: "/Restaurant/List",
        data: { postCode: $("#postCode").val() },
        success: function (data) {
            var json = JSON.parse(data);
            var table = $("#restautanList > tbody:last");
            $.each(json, function (i, item) {
                table.append("<tr>");
                table.append("<td>" + item.Name + "</td>");
                table.append("<td>" + item.AvarageRating + "</td>");
                table.append("<td><img src=\"" + item.Logo + "\"/></td>");
                table.append("<td><a href=\"" + item.WebsiteUrl + "\">" + item.WebsiteUrl + "</a></td>");
                table.append("<td id=\"r-" + item.ID + "\"><div class=\"button\" onclick=\"showProducts('" + item.ID + "');\">Show products</div></td>");
                table.append("</tr>");
                
            });
        }
    });
};

var showProducts = function (restaurantID) {
    var productContent = $("#r-" + restaurantID);
    $.ajax({
        type: 'get',
        url: "/Product/List",
        data: { restaurantID: restaurantID },
        success: function (data) {
            var productList = "<div class=\"productList\">";
            var json = JSON.parse(data);
            $.each(json, function(i, item) {
                productList += item;
                productList += "<br/>";
            });
            productList += "</div>";
            productContent.html(productList);
        }
    });
};

$("#showRestaurantsButton").click(showRestaurants);