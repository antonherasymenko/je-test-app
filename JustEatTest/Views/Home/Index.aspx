﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Main.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server">
    <h2>JustEat test application</h2>
    <link rel="stylesheet" type="text/css" href="/Content/css/index.css">
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="Content" runat="server">
    <%= Html.Label("Enter postcode:") %>
    <%= Html.TextBox("postCode") %>
    <div class="button" id="showRestaurantsButton">
        Show Restaurants
    </div>

    <table id="restautanList" cellspacing="0">
        <caption>List of restaurants</caption>
        <tbody>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Average rating</th>
                <th scope="col">Logo</th>
                <th scope="col">Website</th>
                <th scope="col">Products</th>
            </tr>
        </tbody>
    </table>
</asp:Content>

<asp:Content ID="scriptBlock" ContentPlaceHolderID="footer" runat="server">
    <script type="text/javascript" src="/Content/js/index.js"></script>
</asp:Content>
