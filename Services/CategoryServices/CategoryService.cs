﻿using System.Collections.Generic;
using System.Linq;
using JustEat.Domain.Entities;
using JustEat.Utils;
using Newtonsoft.Json.Linq;

namespace JustEat.Services.CategoryServices
{
    public class CategoryService : BaseService, ICategoryService
    {
        public IEnumerable<ProductCategory> LoadCategories(string menuID)
        {
            var categoryUrl = ApplicationSettings.CategoryListApiUrl(menuID);

            var json = DoRequest(categoryUrl);
            var categories = JObject.Parse(json);

            return categories["Categories"].ToArray().Select(category => new ProductCategory
            {
                ID = category["Id"].ToString()
            }).ToList();
        }
    }
}