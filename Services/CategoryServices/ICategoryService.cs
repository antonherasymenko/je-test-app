﻿using System.Collections.Generic;
using JustEat.Domain.Entities;

namespace JustEat.Services.CategoryServices
{
    public interface ICategoryService
    {
        IEnumerable<ProductCategory> LoadCategories(string menuID);
    }
}