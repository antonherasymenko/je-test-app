﻿using System.Collections.Generic;
using JustEat.Domain.Entities;

namespace JustEat.Services.RestaurantServices
{
    public interface IRestaurantService
    {
        IEnumerable<RestaurantInfo> RestaurantList(string postcode);
    }
}