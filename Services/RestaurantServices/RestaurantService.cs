﻿using System;
using System.Collections.Generic;
using System.Linq;
using JustEat.Domain.Entities;
using JustEat.Utils;
using Newtonsoft.Json.Linq;

namespace JustEat.Services.RestaurantServices
{
    public class RestaurantService : BaseService, IRestaurantService
    {
        public IEnumerable<RestaurantInfo> RestaurantList(string postcode)
        {
            var url = String.Format("{0}?q={1}", ApplicationSettings.RestaurantApiUrl, postcode);
            var result = DoRequest(url);
            return ParseRestaurantInfosJson(result);
        }

        private static IEnumerable<RestaurantInfo> ParseRestaurantInfosJson(string json)
        {
            var jtoken = JObject.Parse(json);
            var restaurants = jtoken["Restaurants"].ToArray();

            var list = restaurants.Select(restaurant => new RestaurantInfo
            {
                ID = restaurant["Id"].ToString(), 
                Name = restaurant["Name"].ToString(), 
                AvarageRating = restaurant["RatingStars"].ToString(), 
                Logo = restaurant["Logo"].ToArray()[0]["StandardResolutionURL"].ToString(), 
                WebsiteUrl = restaurant["Url"].ToString()
            }).ToList();

            return list.OrderByDescending(x => x.AvarageRating);
        }
    }
}