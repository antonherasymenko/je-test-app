﻿using System.Collections.Generic;

namespace JustEat.Services.ProductServices
{
    public interface IProductService
    {
        IEnumerable<string> LoadProductNames(string restaurantID);
    }
}