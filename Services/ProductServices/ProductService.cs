﻿using System.Collections.Generic;
using System.Linq;
using JustEat.Services.CategoryServices;
using JustEat.Utils;
using Newtonsoft.Json.Linq;

namespace JustEat.Services.ProductServices
{
    public class ProductService : BaseService, IProductService
    {
        private readonly ICategoryService _categoryService;
        public ProductService()
        {
            _categoryService = new CategoryService();;
        }

        public IEnumerable<string> LoadProductNames(string restaurantID)
        {
            var result = new List<string>();
            var menuResult = DoRequest(ApplicationSettings.MenuApiUrl(restaurantID));
            var json = JObject.Parse(menuResult);
            foreach (var menuItem in json["Menus"].ToArray())
            {
                var menuID = menuItem["Id"].ToString();
                var productCategories = _categoryService.LoadCategories(menuID);
                foreach (var productCategory in productCategories)
                {
                    var productsByCategory = DoRequest(ApplicationSettings.ProductListApiUrl(menuID, productCategory.ID));
                    var productsJson = JObject.Parse(productsByCategory);

                    result.AddRange(productsJson["Products"].ToArray().Select(product => product["Name"].ToString()));
                }
            }
            return result.Distinct();
        }
    }
}