﻿using System.IO;
using System.Net;

namespace JustEat.Services
{
    public abstract class BaseService
    {
        public string DoRequest(string url)
        {
            string result;
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "GET";
            request.Headers.Add("Accept-Tenant", "uk");
            request.Headers.Add("Accept-Language", "en-GB");
            request.Headers.Add("Accept-Charset", "utf-8");
            request.Headers.Add("Authorization", "Basic VGVjaFRlc3RBUEk6dXNlcjI=");
            using (var response = (HttpWebResponse)request.GetResponse())
            {
                var stream = response.GetResponseStream();
                using (var reader = new StreamReader(stream))
                {
                    result = reader.ReadToEnd();
                }
            }
            return result;
        }
    }
}